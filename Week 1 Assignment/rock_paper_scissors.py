import random

# initialising an empty dictionary to store the details of the game
gameInfo = {}

# assigning numbers to each choice
choices = {
    'rock': 0,
    'paper': 1,
    'scissors': 2
}

# result is 4x3 matrix which represents the possible outcomes of the game
# 0 -> tie, 1 -> player win, 2 -> player lose and 3 -> invalid input
result = [[0,2,1],
        [1,0,2],
        [2,1,0],
        [3,3,3]]


# initial scores of player and computer respectively
pScore = 0
cScore = 0

# list to determine who won the round
winner = ['Tied', 'Player', 'Computer']


for i in range(1,11):

    # getting the inputs and converting them to their index
    player_choice = input("Choose your option: Rock, Paper or Scissors\n").lower() # lower() is used to make inputs consistent
    player_index = choices.get(player_choice, 3)
    computer_choice = random.choice(['rock','paper','scissors'])
    computer_index = choices.get(computer_choice, 3)
    

    # picking the winner of the round using the result matrix
    resultIndex = result[player_index][computer_index]
    roundWin = winner[resultIndex]

    # calculating points and displaying messages for each round
    if resultIndex == 1:
        print("You won the round\n")
        pScore+=1
    elif resultIndex == 2:
        print("You lost the round\n")
        cScore+=1
    else:
        print("The round was tied\n")

    # adding the details of each round into the dictionary
    gameInfo[i]=[player_choice, computer_choice, roundWin]


# printing the final scores of both player and computer
print(f"\nTHE FINAL SCORES\nPlayer = {pScore}\nComputer = {cScore}")

# displaying the winner
print("\nPLAYER WINS" if pScore > cScore else "\nCOMPUTER WINS" if cScore > pScore else "MATCH TIED")


# enabling users to check the details of each round
while True:
    answer = int(input("\nEnter the round for which you need information. Enter 0 to quit\n"))
    if answer == 0:
        break
    try:
        # checking if the game was a tie cause we need to display a different result
        if gameInfo[answer][2] == 'Tied':
            print(f"Round {answer} was tied")
        else:
            print(f"Player Choice = {gameInfo[answer][0]}\nComputer Choice = {gameInfo[answer][1]}\n{gameInfo[answer][2]} won Round {answer}")
    except:
        print("The round you searched for doesn't exist")