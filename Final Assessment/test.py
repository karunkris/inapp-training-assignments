# main class
class Pet:


    def __init__(self, species=None, name = "") -> None:
        self.species = species
        self.name = name

    def __str__(self) -> str:
        if not self.name:
            return f"Species of: {self.species}, unnamed"
        else:
            return  f"Species of: {self.species}, named {self.name}"


# class dog
class Dog(Pet):


    def __init__(self, species, name, chases="Cats") -> None:
        super().__init__(species=species, name=name)
        self.chases = chases

    def __str__(self) -> str:
        if not self.name:
            return f"Species of: Dog, unnamed,chases {self.chases}"
        else:
            return f"Species of: Dog, named {self.name}, chases {self.chases}"



# class cat
class Cat(Pet):


    def __init__(self, species, name, hates="Dogs") -> None:
        super().__init__(species=species, name=name)
        self.hates = hates

    
    def __str__(self) -> str:
        if not self.name:
            return f"Species of: Cat, unnamed, hates {self.hates}"
        else:
            return  f"Species of: Cat, named {self.name}, hates {self.hates}"



# dictionary to store the pet objects
pet_list = dict()

pet_list['Dog'] = []
pet_list['Cat'] = []


print("Enter details of the dog")
for i in range(0,5):
    pet_name = input(f"Enter the name of dog {i+1}\n")
    pet_list['Dog'].append(Dog(species = 'Dog', name = pet_name))




print("Enter details of the cat")
for i in range(0,3):
    pet_name = input(f"Enter the name of dog {i+1}\n")
    pet_list['Cat'].append((Cat(species = 'Cat', name = pet_name)))



# pet_list['Dog'] = [Dog(species='Dog') for i in range(1,6)]
# pet_list['Cat'] = [Cat(species='Cat') for i in range(1,4)]



# while True:
    
#     choice = int(input("Enter 0 -> Quit\nEnter 1 -> Dog\nEnter 2 -> Cat\n"))
#     if choice == 0:
#         break

#     elif choice == 1:
#         select = int(input("You have 5 dogs, select which dog details you want?\n"))
#         pet_species = input("Enter the species of the dog\n")
#         pet_name = input("Enter the name of the pet\n")
#         print(pet_list['Dog'][select])

#     elif choice == 2:
#         select = int(input("You have 3 cats, select which dog details you want?\n"))
#         pet_species = input("Enter the species of the cat\n")
#         pet_name = input("Enter the name of the pet\n")
#         print(pet_list['Cat'][select])

#     else:
#        print("Invalid input\n")