let navExpand = document.querySelector(".nav-expand");
let nav = document.querySelector(".nav");
let navListItem = document.querySelectorAll(".nav-item");

navExpand.addEventListener("click", () => {
  nav.classList.toggle("nav-closed");
});

navListItem.forEach((link) => link.addEventListener("click", listActive));

function listActive() {
  navListItem.forEach((link) => link.classList.remove("nav-item-active"));
  this.classList.add("nav-item-active");
}