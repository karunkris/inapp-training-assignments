import random
import sys

# dictionary that stores the sounds of each pet
sounds = dict()

# contains the name of the pets
pets = dict()

# main class
class Pet:

    # class variables
    boredom_threshold = 6
    hunger_threshold = 6
    boredom_decrement = 2.5
    hunger_decrement = 2.5

    # initialising the variables of each instance
    def __init__(self):
        self.hunger = random.randint(0, Pet.hunger_threshold)
        self.boredom = random.randint(0, Pet.boredom_threshold)

    
    def clock_tick(self):
        self.hunger+=1
        self.boredom+=1

    
    def __str__(self):
        if self.boredom > Pet.boredom_threshold and self.hunger > Pet.hunger_threshold:
            return "Your Pet is Hungry and Bored"
        elif self.boredom > Pet.boredom_threshold:
            return "Your Pet is Bored"
        elif self.hunger > Pet.hunger_threshold:
            return "Your Pet is Hungry"
        else:
            return "Your Pet is Happy"

    
    def feed(self):
        self.reduce_hunger()
        self.clock_tick()

        if self.hunger < 0:
            self.hunger = 0
        

    def hi(self, sound, pet_name):
        self.reduce_boredom()
        self.clock_tick()

        if self.boredom < 0:
            self.boredom = 0
        

        for k,v in sound.items():
            if k == pet_name:
                word = v

        print(random.choice(word))

    def teach(self):
        self.reduce_boredom()
        self.clock_tick()

    
    def reduce_boredom(self):
        self.boredom-=Pet.boredom_decrement

    def reduce_hunger(self):
        self.hunger-=Pet.hunger_threshold



# loop to create the pet names. pet_index -> index of the last pet added
pet_index = 0
while True:
    name = input("Type the names of your pets. Enter 'f' to finish\n")
    if name == 'f':
        break
    pets[pet_index] = name
    pet_index+=1


# creating pet instances. This is a list that represents each pet
pet_instance = [Pet() for i in range(len(pets))]



# MAIN PROGRAM
while True:

    # getting the pet names from the dictionary
    values = list(pets.values())

    # asking the user to select his pet
    while True:
        choice = int(input("""
        Enter 0 -> Quit
        Enter 1 -> To add new pet
        Enter 2 -> To interact with existing pet\n"""))
        
        if choice == 0:
            sys.exit() # to terminate the program

        elif choice == 1:
            new_pet = input("Enter the name of the new pet\n")
            pet_index+=1
            pets[pet_index] = new_pet
            pet_instance.append(Pet())

        elif choice == 2:

            # user chooses the pet to interact with
            while True:
                print(values)
                name = input("From the above, choose which pet you want to interact with\n")
                if name not in values:
                    print("Pet doesn't exist. Choose again\n")
                else:
                    break

            # user interactivity with pet
            while True:
                choice = int(input("""
                Enter 0 -> Go back to main menu
                Enter 1 -> To greet your pet
                Enter 2 -> To teach a new word
                Enter 3 -> To feed your pet
                Enter 4 -> To get status of pet\n"""))

                
                # to go back to pet selection
                if choice == 0:
                    break
                    

                # calling the method hi() to greet the pet
                elif choice == 1:
                    index = values.index(name)

                    # code for time passing
                    for i in range(len(pet_instance)):
                        pet_instance[i].clock_tick()

                    if name in sounds.keys():
                        pet_instance[index].hi(sounds, name)
                    
                    else:
                        print(f"{name} doesn't know any words\n")

                
                # teaching a new sound
                elif choice == 2:
                    word = input(f"What word would you like to teach {name}?\n")

                    # code for time passing
                    for i in range(len(pet_instance)):
                        pet_instance[i].clock_tick()
                    
                    sounds.setdefault(name, [])
                    sounds[name].append(word)

                # feeding the pet
                elif choice == 3:
                    index = values.index(name)
                    pet_instance[index].feed()

                    # code for time passing
                    for i in range(len(pet_instance)):
                        pet_instance[i].clock_tick()

                # printing the status of the pet
                elif choice == 4:
                    index = values.index(name)
                    print(pet_instance[index])
                
                else:
                    print("Invalid Choice\n")