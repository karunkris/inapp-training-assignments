import sqlite3

# connecting to the database
conn = sqlite3.connect('Week 4 Assignment/Assignment 3/database.sqlite')

# creating the cursor
cur = conn.cursor()


# counting number of rows
cur.execute("SELECT COUNT(*) FROM Teams")
items = cur.fetchone()
print(items[0])


print("-------------------\n")


# counting distinct seasons
cur.execute("SELECT DISTINCT Season FROM Teams")
items = cur.fetchall()
for item in items:
    print(item[0])


print("-------------------\n")


# smallest and largest stadium capacity
cur.execute("SELECT min(StadiumCapacity) FROM Teams")
minimum = cur.fetchone()
cur.execute("SELECT max(StadiumCapacity) FROM Teams")
maximum = cur.fetchone()
print(f"Minimum -> {minimum[0]}\nMaximum -> {maximum[0]}")


print("-------------------\n")


# sum of squad players
cur.execute("SELECT sum(KaderHome) FROM Teams WHERE Season = 2014")
item = cur.fetchone()
print(item[0])


print("-------------------\n")


# MANU goal scored on average
cur.execute("SELECT avg(FTHG) FROM Matches WHERE HomeTeam = 'Man United'")
item = cur.fetchone()
print(item[0])


# commiting the changes
conn.commit()

# closing the connection
conn.close()