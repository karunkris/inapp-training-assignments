import sqlite3

# connecting to the database
conn = sqlite3.connect('Week 4 Assignment/Assignment 3/database.sqlite')

# creating the cursor
cur = conn.cursor()


# a)
cur.execute("""SELECT HomeTeam, FTHG, FTAG FROM Matches
                WHERE Season = 2010 AND HomeTeam = 'Aachen'
                ORDER BY FTHG DESC""")
items = cur.fetchall()
for item in items:
    print(item)


print("-------------------\n")


# b)
cur.execute("""SELECT HomeTeam, COUNT(*) FROM Matches
                WHERE Season = 2016 AND FTR = 'H'
                GROUP BY HomeTeam
                ORDER BY COUNT(*) DESC""")
items = cur.fetchall()
for item in items:
    print(item)


print("-------------------\n")


# c)
cur.execute("""SELECT * FROM Unique_Teams
                LIMIT 10""")
items = cur.fetchall()
for item in items:
    print(item)


print("-------------------\n")


# d)
cur.execute("""SELECT Match_ID, Unique_Teams.Unique_Team_ID, TeamName
                FROM Teams_in_Matches, Unique_Teams
                WHERE Teams_in_Matches.Unique_Team_ID = Unique_Teams.Unique_Team_ID
                """)
items = cur.fetchall()
for item in items:
    print(item)


print("-------------------\n")


# e)
cur.execute("""SELECT * FROM Unique_Teams CROSS JOIN Teams
                LIMIT 10
                """)
items = cur.fetchall()
for item in items:
    print(item)


print("-------------------\n")


# f)
cur.execute("""SELECT Unique_Teams.TeamName, Unique_Team_ID, AvgAgeHome, Season, ForeignPlayersHome
                FROM Teams CROSS JOIN Unique_Teams
                LIMIT 10
                """)
items = cur.fetchall()
for item in items:
    print(item)


print("-------------------\n")


# g)
cur.execute("""SELECT max(Matches.Match_ID), Teams_in_Matches.Unique_Team_ID, Unique_Teams.TeamName
                FROM Matches INNER JOIN Teams_in_Matches ON Matches.Match_ID = Teams_in_Matches.Match_ID
                INNER JOIN Unique_Teams ON Teams_in_Matches.Unique_Team_ID = Unique_Teams.Unique_Team_ID
                WHERE Unique_Teams.TeamName LIKE '%y' OR '%r'
                GROUP BY Teams_in_Matches.Unique_Team_ID
                """)
items = cur.fetchall()
for item in items:
    print(item)


# commiting the changes
conn.commit()

# closing the connection
conn.close()