import sqlite3

# connecting to the database
conn = sqlite3.connect('Week 4 Assignment/Assignment 3/database.sqlite')

# creating the cursor
cur = conn.cursor()


# printing details of home and away teams where fthg = 5
cur.execute("SELECT HomeTeam, AwayTeam FROM Matches WHERE FTHG = 5")
items = cur.fetchall()
for item in items:
    print(item)


print("-------------------\n")


# printing details of Arsenal and ftr = 'A'
cur.execute("SELECT * FROM Matches WHERE HomeTeam = 'Arsenal' AND FTR = 'A'")
items = cur.fetchall()
for item in items:
    print(item)


print("-------------------\n")


# printing details of 2012 season till the 2015 season where Away Team is Bayern Munich and FTHG > 2
cur.execute("SELECT * FROM Matches WHERE Season BETWEEN 2012 AND 2015 AND AwayTeam = 'Bayern Munich' AND FTHG > 2")
items = cur.fetchall()
for item in items:
    print(item)


print("-------------------\n")


# printing details of matches where the Home Team name begins with “A” and Away Team name begins with “M”
cur.execute("SELECT * FROM Matches WHERE HomeTeam LIKE 'A%' AND AwayTeam LIKE 'M%'")
items = cur.fetchall()
for item in items:
    print(item)


# commiting the changes
conn.commit()

# closing the connection
conn.close()