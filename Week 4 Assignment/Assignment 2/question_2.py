import sqlite3

# connecting to the database
conn = sqlite3.connect('Week 4 Assignment/Assignment 2/employee.db')

# creating the cursor
cur = conn.cursor()


# creating the department table
cur.execute("""CREATE TABLE IF NOT EXISTS department (
        department_id integer PRIMARY KEY,
        depart_name text
)""")


# department table data
department_details = [
    (1, 'IT'),
    (2, 'HR'),
    (3, 'Sales'),
    (4, 'Services'),
    (5, 'Customer Relations')
]

# inserting the data
cur.executemany("INSERT INTO department VALUES (?,?)", department_details)


# main code
while True:
    choice = int(input("Enter department ID from (1-5). Enter 0 to quit\n"))
    
    if choice == 0:
        break

    # getting the data from table
    cur.execute("SELECT * FROM employee WHERE department_id = ?", (choice,))
    items = cur.fetchall()
    cur.execute("SELECT depart_name FROM department WHERE department_id = ?", (choice,))
    dep_name = cur.fetchone()

    # printing out the data
    for item in items:
        print(f"{item[0]} {item[1]} {item[2]} {item[3]} {item[4]} {dep_name[0]}")


# commiting the changes
conn.commit()

# closing the connection
conn.close()