import sqlite3

# connecting to the database
conn = sqlite3.connect('Week 4 Assignment/Assignment 2/employee.db')

# creating the cursor
cur = conn.cursor()


# creating the employee table
cur.execute("""CREATE TABLE IF NOT EXISTS employee (
        e_name text,
        eid integer PRIMARY KEY,
        salary integer,
        department_id integer,
        FOREIGN KEY (department_id) REFERENCES department(department_id)
)""")


# adding a new column to the table
cur.execute("""ALTER TABLE employee
            ADD COLUMN city text""")



# details of the employee
employee_details = [
    ('Sam', 101, 30000, 1, 'Kochi'),
    ('Jackson', 102, 35000, 1, 'London'),
    ('Joel', 103, 25000, 2, 'Trivandrum'),
    ('Ram', 104, 40000, 2, 'Delhi'),
    ('Manu', 105, 55000, 3, 'Trivandrum')
]


# inserting the values into the table
cur.executemany("INSERT INTO employee VALUES (?,?,?,?,?)", employee_details)


# MAIN CODE
while True:
    choice = int(input("\nEnter 0 -> Quit\nEnter 1 -> Find the details of employees whose name starts with a particular letter\nEnter 2 -> Find details based on ID\nEnter 3 -> Update name based on ID\n"))
    
    if choice == 0:
        break
    
    elif choice == 1:
        letter = input("Enter the first letter of employee you want to search\n")

        cur.execute("SELECT * FROM employee WHERE e_name LIKE ?", (f"{letter}%",))
        print(cur.fetchall())
    
    elif choice == 2:
        e_id = int(input("Choose the ID to search for (101 - 105)\n"))

        cur.execute("SELECT * FROM employee WHERE eid = ?", (e_id,))
        print(cur.fetchall())

    elif choice == 3:
        e_id = int(input("Choose the ID to modify (101 - 105)\n"))
        name = input("Choose the new name\n")

        placeholder = (name, e_id)
        query = "UPDATE employee SET e_name = ? WHERE eid = ?"

        cur.execute(query, placeholder)
        
        cur.execute("SELECT * FROM employee WHERE eid = ?", (e_id,))
        print(cur.fetchone())

    else:
        print("Invalid Input")


# commiting the changes
conn.commit()

# closing the connection
conn.close()