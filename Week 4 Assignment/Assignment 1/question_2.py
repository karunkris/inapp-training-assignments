import sqlite3

# connecting to the database
conn = sqlite3.connect('Week 4 Assignment/Assignment 1/hospital.db')

# creating the cursor
cur = conn.cursor()


# creating the hospital table
cur.execute("""CREATE TABLE IF NOT EXISTS hospital (
    hospital_id integer PRIMARY KEY,
    hospital_name text,
    bed_count integer
)
""")


# creating the doctor table
cur.execute("""CREATE TABLE IF NOT EXISTS doctor (
    doctor_id integer PRIMARY KEY,
    doctor_name text,
    hospital_id integer,
    joining_date blob,
    speciality text,
    salary integer,
    experience null,
    FOREIGN KEY (hospital_id) REFERENCES hospital(hospital_id)
    )
""")


# data of hospital
hospital_details = [
    (1, "Mayo Clinic", 200),
    (2, "Cleveland Clinic", 400),
    (3, "John Hopkins", 1000),
    (4, "UCLA Medical Center", 1500),
]


# data of doctor table
doctor_details = [
    (101, "David", 1, '2005-02-10', "Pediatric", 40000, None),
    (102, "Michael", 1, '2018-07-23', "Oncologist", 20000, None),
    (103, "Susan", 2, '2016-05-19', "Garnacologist", 25000, None),
    (104, "Robert", 2, '2017-12-28', "Pediatric", 28000, None),
    (105, "Linda", 3, '2004-06-04', "Garnacologist", 42000, None),
    (106, "William", 3, '2012-09-11', "Dermatologist", 30000, None),
    (107, "Richard", 4, "2014-08-21", "Garnacologist", 32000, None),
    (108, "Karen", 4, "2011-10-17", "Radiologist", 30000, None)
]


# inserting data into the tables
cur.executemany("INSERT INTO hospital VALUES (?,?,?)", hospital_details)
cur.executemany("INSERT INTO doctor VALUES (?,?,?,?,?,?,?)", doctor_details)


# MAIN CODE
while True:
    choice = int(input("\nEnter 0 -> Quit\nEnter 1 -> Doctor details as per speciality and salary\nEnter 2 -> List of doctors in the hospital\n"))
    
    if choice == 0:
        break
    
    elif choice == 1:
        special = input("Enter the speciality of the doctor\n")
        salary = int(input("Enter the minimum salary of the doctor\n"))
        
        placeholder = (special, salary)
        query = "SELECT * FROM doctor WHERE speciality = ? AND salary > ?"
        cur.execute(query, placeholder)
        
        print(cur.fetchall())
    
    elif choice == 2:
        id = int(input("Enter the hospital ID [1,2,3,4]\n"))
        
        # executing the query using inner join
        cur.execute("""SELECT doctor_name, hospital_name
        FROM doctor INNER JOIN hospital
        ON hospital.hospital_id = doctor.hospital_id
        WHERE doctor.hospital_id = ?
        """, (id,))

        print(cur.fetchall())

    else:
        print("Invalid Input")


# commiting the changes
conn.commit()

# closing the connection
conn.close()