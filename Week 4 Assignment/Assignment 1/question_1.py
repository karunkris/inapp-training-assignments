import sqlite3

# connecting to the database
conn = sqlite3.connect('Week 4 Assignment/Assignment 1/car.db')

# creating the cursor
cur = conn.cursor()


# creating the cars table
cur.execute("""CREATE TABLE cars (
        car_name text,
        car_owner text
)""")


# list storing the details of th cars table
car_details = list()


# getting the details of each car
for i in range(0,10):
    name = input(f"Enter the name of car {i+1}\n")
    owner = input("Enter the name of its owner\n")
    car_details.append((name,owner))


# inserting car details into table
cur.executemany("INSERT INTO cars VALUES (?,?)", car_details)


# fetching the data from the table
cur.execute("SELECT * FROM cars")
items = cur.fetchall()


# printing the data in a tabular format
print("Car Name | Car Owner\n--------------------")
for item in items:
    print(f"{item[0]} {item[1]}")


# commiting the changes
conn.commit()

# closing the connection
conn.close()