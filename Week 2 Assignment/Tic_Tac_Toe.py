import random 

# dictionary to store gamelog
history = dict()

# function to display board
def display_board(board):
    for i in range(1,10,3):
        print(f"\n{board[i]} | {board[i+1]} | {board[i+2]}")


# function for choosing the symbols
def player_input():
    marker = ''
    
    while not (marker == 'X' or marker == 'O'):
        marker = input('Player: Do you want to use X or O? ').upper()

    if marker == 'X':
        return ('X', 'O')
    else:
        return ('O', 'X')


# function to check if the winning condition is satisfied
def win_check(board,mark):
    
    return ((board[7] == mark and board[8] == mark and board[9] == mark) or # across the top
    (board[4] == mark and board[5] == mark and board[6] == mark) or # across the middle
    (board[1] == mark and board[2] == mark and board[3] == mark) or # across the bottom
    (board[7] == mark and board[4] == mark and board[1] == mark) or # down the middle
    (board[8] == mark and board[5] == mark and board[2] == mark) or # down the middle
    (board[9] == mark and board[6] == mark and board[3] == mark) or # down the right side
    (board[7] == mark and board[5] == mark and board[3] == mark) or # diagonal
    (board[9] == mark and board[5] == mark and board[1] == mark)) # diagonal


# function to choose if Player 1 or Player 2's chance
def choose_first():
    if random.randint(0, 1) == 0:
        return 'Computer'
    else:
        return 'Player'

#function to store history of the games & rounds
def store_history(gno, rno, board): 
    history[gno][rno] = board[:]

# function to print history
def print_history(history, index):
    for i in range(1, 10):
        if i in history[index].keys():
            print(f"\nRound-{i}")
            display_board(history[index][i])
    print(f"\n{history[index]['winner']} won Game-{index}")


# MAIN PROGRAM
game_number = 1
print('Welcome to Tic Tac Toe!')
no_of_games = int(input("How many games do you want to play? "))
    
while game_number <= no_of_games:

    # Reset the game board and determine who goes first
    print(f"\nGAME {game_number}")

    # declaring the game log, the game board and the possible positions for marking
    history[game_number] = {}
    theBoard = ['','1','2','3','4','5','6','7','8','9']
    possibleChoices = list(range(1,10))

    player_marker, computer_marker = player_input()

    # selecting the turn
    turn = choose_first()
    print(turn + ' will go first.')

    round_number = 0
    while True:
        if turn == 'Player':
            round_number += 1

            # Player's turn
            display_board(theBoard)
            while True:
                player_choice = int(input("Select a position to mark your symbol\n"))
                if player_choice in possibleChoices:
                    theBoard[player_choice] = player_marker
                    possibleChoices.remove(player_choice)
                    break
                else:
                    print("Invalid input. Choose another position\n")

            store_history(game_number, round_number ,theBoard)

            if win_check(theBoard, player_marker):
                print('\nCongratulations! You have won the game!')
                history[game_number]["winner"] = "Player"
                display_board(theBoard)
                break
            else:
                if round_number == 9:
                    print('The game is a draw!')
                    history[game_number]["winner"] = "Tie"
                    display_board(theBoard)
                    break
                else:
                    turn = 'Computer'
            
        else:
            round_number += 1
            print("\nComputer's Turn\n")
            if possibleChoices:
                computer_choice = random.choice(possibleChoices)
                theBoard[computer_choice] = computer_marker
                possibleChoices.remove(computer_choice)

            display_board(theBoard)
            store_history(game_number, round_number ,theBoard)

            if win_check(theBoard, computer_marker):
                print('Computer has won!')
                history[game_number]["winner"] = "Computer"
                display_board(theBoard)
                break
            else:
                if round_number == 9:
                    print('The game is a draw!')
                    history[game_number]["winner"] = "Tie"
                    display_board(theBoard)
                    break
                else:
                    turn = 'Player'
            
    game_number += 1

# displaying round information
while True:
    answer = int(input("\nEnter the round for which you need information. Enter 0 to quit\n"))
    if answer == 0:
        break
    try:
        print_history(history, answer)
    except:
        print("The round you searched for doesn't exist")